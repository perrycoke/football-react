import { FETCH_MATCHES, FETCH_MATCH, FETCH_LEAGUES } from '../actions/types';
  
export default ( state = {}, action ) => {
    switch ( action.type ) {
        case FETCH_MATCH:
            return { ...state, [ action.payload.id ]: action.payload };
        case FETCH_MATCHES:
            return { ...state, matches: action.payload.api.fixtures };
        case FETCH_LEAGUES:
            return { ...state, leagues: action.payload.api.leagues };
        default:
            return state;
    }
};
  
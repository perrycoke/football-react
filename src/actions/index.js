import axios from 'axios';
import { BASE_API } from '../config/constants';
import { FETCH_MATCHES, FETCH_MATCH, FETCH_LEAGUES } from './types';
const HEADERS = {
    headers: {
        'x-rapidapi-host': 'api-football-v1.p.rapidapi.com',
        'x-rapidapi-key': 'Hwt2fbPFoUBzYfCvwNcWAxx1l6wgMA3O'
    }
};

export const fetchLeagues = () => async dispatch => {
    const response = await axios.get( `${ BASE_API }/leagues/country/england/2019`, HEADERS );

    dispatch( { type: FETCH_LEAGUES, payload: response.data } );
};

export const fetchMatches = id => async dispatch => {
    const response = await axios.get( `${ BASE_API }/fixtures/league/${ id }/last/10?timezone=Europe%2FLondon`, HEADERS );

    dispatch( { type: FETCH_MATCHES, payload: response.data } );
};

export const fetchMatch = id => async dispatch => {
    const response = await axios.get(`streams/${id}`);

    dispatch({ type: FETCH_MATCH, payload: response.data });
};

export const FETCH_MATCHES = 'FETCH_MATCHES';
export const FETCH_MATCH = 'FETCH_MATCH';
export const FETCH_LEAGUE = 'FETCH_LEAGUE';
export const FETCH_LEAGUES = 'FETCH_LEAGUES';

import axios from 'axios';
// import fixturesApi from '../apis/fixturesApi';
import { FETCH_LEAGUES, FETCH_LEAGUE } from './types';
import { BASE_API } from '../config/constants';
const baseUrl = `${ BASE_API }/leagues/country/england/2019`;
const HEADERS = {
    headers: {
        'x-rapidapi-host': 'api-football-v1.p.rapidapi.com',
        'x-rapidapi-key': 'Hwt2fbPFoUBzYfCvwNcWAxx1l6wgMA3O'
    }
}

export const fetchLeagues = () => async dispatch => {
    const response = await axios.get( baseUrl, HEADERS );

    dispatch( { type: FETCH_LEAGUES, payload: response.data } );
};

export const fetchLeague = id => async dispatch => {
    const response = await axios.get( `streams/${id}` );

    dispatch( { type: FETCH_LEAGUE, payload: response.data } );
};

import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import Leagues from './leagues';
import Matches from './matches';
import history from '../history';

function App() {
    return (
        <div>
            <header>Football League</header>
            <Router history={ history }>
                <Switch>
                    <Route path="/" exact component={ Leagues } />
                    <Route path="/matches/:id" exact component={ Matches } />
                </Switch>
            </Router>
        </div>
    );
}

export default App;

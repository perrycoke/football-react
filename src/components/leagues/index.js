import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchLeagues } from '../../actions';

class Leagues extends React.Component {
    componentDidMount() {
        this.props.fetchLeagues();
    }

    renderLeagues() {
        const leagues = this.props.leagues.filter( ( league, index ) => index < 4 );
        return leagues.map( league => {
            return (
                <div className="leagues__item" key={ league.league_id }>
                    <Link to={ `/matches/${ league.league_id }` }>
                        <img
                            className="league__logo"
                            src={ league.logo }
                            alt={ league.name }
                        />
                        <span className="league__name">{ league.name }</span>
                    </Link>
                </div>
            );
        } );
    }

    render() {
        if ( !this.props.leagues ) {
            return <div>Loading...</div>;
        }

        return (
            <div>
                <h2>Leagues</h2>

                <div className="leagues">{ this.renderLeagues() }</div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        leagues: state.leagues,
    };
};

export default connect( mapStateToProps, { fetchLeagues } )( Leagues );

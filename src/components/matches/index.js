import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchMatches } from '../../actions';

class Matches extends React.Component {
    componentDidMount() {
        this.props.fetchMatches();
    }

    renderMatches() {
        return this.props.matches.map( match => {
            return (
                <div className="match" key={ match.fixture_id }>
                    <div className="match__team">
                        <img className="match__team-logo" src={ match.homeTeam.logo } alt={ match.homeTeam.team_name } />
                        <span className="match__team-name">{ match.homeTeam.team_name }</span>
                    </div>

                    <div className="match__score">

                    </div>

                    <div className="match__team">
                        <img className="match__team-logo" src={ match.awayTeam.logo } alt={ match.awayTeam.team_name } />
                        <span className="match__team-name">{ match.awayTeam.team_name }</span>
                    </div>
                </div>
            );
        } );
    }

    render() {
        if ( !this.props.matches ) {
            return <div>Loading...</div>;
        }

        return (
            <div>
                <h2>Matches</h2>

                <div className="ui celled list">
                    { this.renderMatches() }
                </div>
            </div>
        );
    }
};

const mapStateToProps = state => {
    return {
        matches: state.matches
    };
};

export default connect( mapStateToProps, { fetchMatches } )( Matches );

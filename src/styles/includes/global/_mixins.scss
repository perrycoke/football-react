// Mixins

@mixin clearfix { // Stops element with floated children from collapsing

    &::before {
        content: '';
        display: block;
    }

    &::after {
        content: '';
        display: table;
        clear: both;
    }
}

/*
 * When a full-width widget is inside a container, this mixin uses viewport width to break it out of that container
 * Note that overflow-x: hidden should be applied to to <body>, as the vw calculation does not take into account static scrollbar width
 */

@mixin full-width-widget {
    width: 100%;

    @supports (width: 100vw) {
        width: 100vw;
        position: relative;
        left: 50%;
        right: 50%;
        margin-left: -50vw;
        margin-right: -50vw;

        @media (max-width: $break-tablet) {
            width: auto;
            margin-left: -2rem;
            margin-right: -2rem;
            left: auto;
            right: auto;
        }

        @media (max-width: $break-phablet) {
            margin-left: -1rem;
            margin-right: -1rem;
        }
    }
}

@mixin link-reset { // remove default browser styling of a link tag
    color: inherit;
    text-decoration: none;
}

@mixin button-reset { // remove default browser styling of a button tag
    background: transparent;
    border: 0;
    outline: 0;
    padding: 0;
}

/*
 * Adds hover and focus styles for tablet (by default) and up
 * Only keeps focus styles for below tablet (by default)
 * If focus set to false then only hover styling will be applied
 */

@mixin hover($breakpoint: tablet, $focus: true) {

    @if $focus == true {

        &:focus {
            @content;
        }
    }

    @include mq($from: $breakpoint) {

        &:hover {
            @content;
        }
    }
}

@mixin retina-bg($file, $type, $width, $height) {
    // stylelint-disable-next-line function-url-quotes
    background-image: url($file + '.' + $type);

    @media (-webkit-min-device-pixel-ratio: 2), (-moz-min-device-pixel-ratio: 2) {

        & {
            // stylelint-disable-next-line function-url-quotes
            background-image: url($file + '@2x.' + $type);
            background-size: $width $height;
        }
    }
}

@mixin retina-bg-cover($file, $type) {
    // stylelint-disable-next-line function-url-quotes
    background-image: url($file + '.' + $type);
    background-size: cover;
    background-repeat: no-repeat;

    @media (-webkit-min-device-pixel-ratio: 2), (-moz-min-device-pixel-ratio: 2) {

        & {
            // stylelint-disable-next-line function-url-quotes
            background-image: url($file + '@x2.' + $type);
        }
    }
}

/*
 * Gives a container with a fixed position child element equal dimensions
 * to prevent content from moving behind
 */

@mixin fixed-container($fixed-container-height) {
    width: 100%;
    height: $fixed-container-height;
    display: block;
    position: relative;
    top: 0;
}

/*
 * Applies an ellipsis to text that overflows the width passed into the mixin
 * NB this ONLY works on single lines of text
 */

@mixin truncate($width: 100%) {
    width: $width;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}

/*
  This mixin can be used to set the object-fit:
  @include object-fit(contain);
  or object-fit and object-position:
  @include object-fit(cover, top);
  https://github.com/bfred-it/object-fit-images
*/

@mixin object-fit($fit: fill, $position: null) {
    // stylelint-disable-next-line property-no-vendor-prefix
    -o-object-fit: $fit;
    object-fit: $fit;

    @if $position {
        // stylelint-disable-next-line property-no-vendor-prefix
        -o-object-position: $position;
        object-position: $position;
        font-family: 'object-fit: #{$fit}; object-position: #{$position}';
    } @else {
        font-family: 'object-fit: #{$fit}';
    }
}

/// Set items into a manually scrollable horizontal row
/// @param {String} $parent - class name of parent element (e.g. '.video-list')
/// @param {Integer} $item-width - fixed width of each item
/// @param {String} $gutter-width - width between items
/// @param {String} $edge-offset - width of the gutter outside widget (to allow row to sit flush with edge of screen)
/// @group mixins

@mixin manual-scroll( $parent, $item-width, $gutter-width, $edge-offset: 0 ) {

    #{$parent} {

        &__list {
            display: flex;
            flex-wrap: nowrap;
            margin-bottom: 0;
            margin-left: -$gutter-width;
            margin-right: -$gutter-width;
            overflow-x: auto;
            position: relative;
            -webkit-overflow-scrolling: touch;
            scroll-snap-type: x mandatory;

            &::after {
                content: '';
                display: block;
                flex: 0 0 auto;
                height: 1rem;
                width: $gutter-width;
            }
        }

        &__item {
            display: flex; // keep all item heights equal
            margin-right: 0;
            margin-left: 0;
            padding-left: $gutter-width;
            min-width: $item-width;
            scroll-snap-align: start;
            width: $item-width;

            &:last-of-type {
                margin-right: 0;
            }
        }
    }
}

// Helper mixins for the manual-scroll mixin

@mixin manual-scroll-list( $gutter-width ) {
    display: flex;
    flex-wrap: nowrap;
    margin-bottom: 0;
    margin-left: -$gutter-width;
    margin-right: -$gutter-width;
    overflow-x: auto;
    position: relative;
    -webkit-overflow-scrolling: touch;
    scroll-snap-type: x mandatory;

    &::after {
        content: '';
        display: block;
        flex: 0 0 auto;
        height: 1rem;
        width: $gutter-width;
    }
}

@mixin manual-scroll-item( $gutter-width, $item-width ) {
    display: flex; // keep all item heights equal
    margin-right: 0;
    margin-left: 0;
    padding-left: $gutter-width;
    min-width: $item-width;
    scroll-snap-align: start;
    width: $item-width;

    &:last-of-type {
        margin-right: 0;
    }
}

@mixin text-gradient( $gradient: null ) {
    @if $gradient {
        background-image: $gradient;
    } @else {
        background-image: $mc-text-gradient;
    }
    -webkit-background-clip: text; /* stylelint-disable-line property-no-vendor-prefix */
    background-clip: text;
    -webkit-text-fill-color: transparent;
}

//Helpers mixins for the telstra bar which scope the containing styles to the site variation (AFL/CLUB/OFF-NETWORK)

@mixin th-club($toggleClass: '') {

    .th--club #{$toggleClass} & {
        @content;
    }
}

@mixin th-off-network($toggleClass: '') {

    .th--off-network #{$toggleClass} & {
        @content;
    }
}

@mixin th-afl($toggleClass: '') {

    .th--afl #{$toggleClass} & {
        @content;
    }
}

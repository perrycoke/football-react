import fs from 'fs';
import path from 'path';
import s3 from 'gulp-s3';
import es from 'event-stream';
import gitmodified from 'gulp-gitmodified';
import { gitHash } from './helpers/gitHash.js';
import { publishFiles } from './helpers/publish-files.js';
import rename from 'gulp-rename';
import count from 'gulp-count';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import awspublish from 'gulp-awspublish';
import config from './config.json';

function getFolders(dir){
    return fs.readdirSync(dir)
      .filter(function(file){
        return fs.statSync(path.join(dir, file)).isDirectory();
      });
}

var getDeploymentTasks = function( folders, AWSConfig, versionAppendString, deployAll ) {
    var tasks = [];
	folders.forEach(function(folder){
		var sitePath = AWSConfig.path[ folder ];

        gitHash().then( hash => {
            // check we have a valid upload path
            if( sitePath ) {
                // deployment naming is based on the current git hash
                var calcPath = path.join( sitePath, hash + ( versionAppendString || "" ) ) + "/";
                var source = path.join( path.join( config.dist.freemarker, folder ), '*.ftl' );
                // since we have a valid deploy location, add this task to the stream
                if( deployAll ){
                    tasks.push(
                        gulp.src( source )
                        .pipe( rename( filepath => {
                            filepath.dirname = path.join( calcPath, filepath.dirname );
                        } ) )
                        .pipe( publishFiles(
                            config.s3.platform.profile,
                            config.s3.platform.region,
                            config.s3.platform.bucket
                        ) )
                        .pipe( awspublish.reporter() )
                        .pipe( count( 'Deployed ## widgets' ) )
                    );
                } else {
                    tasks.push(
                        gulp.src( source )
                        .pipe(gitmodified({
                            modes: ['added', 'modified']
                        }))
                        .pipe( rename( filepath => {
                            filepath.dirname = path.join( calcPath, filepath.dirname );
                        } ) )
                        .pipe( publishFiles(
                            config.s3.platform.profile,
                            config.s3.platform.region,
                            config.s3.platform.bucket
                        ) )
                        .pipe( awspublish.reporter() )
                        .pipe( count( 'Deployed ## widgets' ) )
                    );
                }
            } else {
                console.log( 'deploy:bundle:dev - no s3 path config found for site: ' + folder );
            }
        } );
	});

    return tasks;
}

gulp.task( 'deploy:bundle:dev', [ 'ftl' ], function() {

    var folders = getFolders(config.dist.freemarker);
    var appendString = "_" + Math.floor( new Date().getTime() / 1000 );
    var deployTasks = getDeploymentTasks( folders, config.s3.platform.dev, appendString );

    return es.concat.apply(null, deployTasks);
} );

gulp.task( 'deploy:bundle:dev:all', [ 'ftl' ], function() {

    var folders = getFolders(config.dist.freemarker);
    var appendString = "_" + Math.floor( new Date().getTime() / 1000 );
    var deployTasks = getDeploymentTasks( folders, config.s3.platform.dev, appendString, true );

    return es.concat.apply(null, deployTasks);
} );

gulp.task( 'deploy:bundle:test', [ 'ftl' ], function() {

    var folders = getFolders(config.dist.freemarker);
    var appendString = "_" + Math.floor( new Date().getTime() / 1000 );
    var deployTasks = getDeploymentTasks( folders, config.s3.platform.test, appendString );

    return es.concat.apply(null, deployTasks);
} );

gulp.task( 'deploy:bundle:test:all', [ 'ftl' ], function() {

    var folders = getFolders(config.dist.freemarker);
    var appendString = "_" + Math.floor( new Date().getTime() / 1000 );
    var deployTasks = getDeploymentTasks( folders, config.s3.platform.test, appendString, true );

    return es.concat.apply(null, deployTasks);
} );

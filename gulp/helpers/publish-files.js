import AWS from 'aws-sdk';
import awspublish from 'gulp-awspublish';
import parallelize from 'concurrent-transform';
import config from '../config.json';

/**
 * Establishes S3 connection and uploads files
 * @param  {String} profile     the AWS profile (e.g, platform-s3)
 * @param  {String} region      AWS region (e.g., us-east-1)
 * @param  {String} bucket      name of the bucket where files get uploaded in S3
 * @return {Function}           AWS SDK helper for publishing
 */
export const publishFiles = ( profile, region, bucket ) => {

    process.env.AWS_PROFILE = profile;
    process.env.AWS_SDK_LOAD_CONFIG = "true";

    return parallelize( awspublish.create( {
        region: region,
        params: {
            Bucket: bucket
        },
        credentials: AWS.config.credentials
    } ).publish(), 40 );
};
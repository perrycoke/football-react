import fs from 'fs';
import path from 'path';
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import rename from 'gulp-rename';
import uglify from 'gulp-uglify';
import sourcemaps from 'gulp-sourcemaps';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from '../config.json';
gulpParam(gulp, process.argv);

export function buildWidget( folder, widget, isProd = false, dist = false, basePath = config.src.sites, includeTemplates = true ) {

    const structurePath = path.join(path.join(basePath, folder), widget, 'js/_structure.txt');

    const jsSrc = includeTemplates ? [ path.join(path.join(basePath, folder), widget, 'concat_tmpl/concat.templates.js') ] : [];

    let hasStructure = fs.existsSync(structurePath);

    if ( hasStructure ) {
        console.log(folder + '_' + widget + ': using _structure.txt');
        let structure = fs.readFileSync(structurePath, 'utf8').split('\n');
        for ( let i = 0; i < structure.length; i++ ) {
            path.join(  )
            jsSrc.push(path.join(path.join(basePath, folder), widget, structure[ i ]));
        }
    } else {
        jsSrc.push(path.join(path.join(basePath, folder), widget, '**/*.js'));
    }

    if ( isProd ) {
        return gulp.src(jsSrc)
            .pipe(babel())
            .pipe(concat(folder + '_' + widget + '.js'))
            // .pipe(gulp.dest(config.dist.widgets))
            .pipe(rename(folder + '_' + widget + '.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest(dist||config.dist.widgets));
    } else {
        return gulp.src(jsSrc)
            .pipe(sourcemaps.init())
            .pipe(babel())
            .pipe(concat(folder + '_' + widget + '.js'))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(dist||config.dist.widgets));
    }
};

import git from 'git-rev';

export async function gitHash() {
    let promise = new Promise( resolve => {
        git.short( str => {
            resolve( str );
        } );
    } );

    let hash = await promise;
    return hash;
}

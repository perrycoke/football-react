import path from 'path';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import insert from 'gulp-insert';
import escape from 'gulp-js-escape';
import flatmap from 'gulp-flatmap';
import htmlmin from 'gulp-htmlmin';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
gulpParam(gulp, process.argv);


export function getTemplateDeclarations( folder, widget, basepath ) {
	const underscorePathRexp = new RegExp( /(\\|\/)([\w\-_]+.html)/ );

	//look for any HTML template files in the widget directory
	return gulp.src( path.join( path.join(path.join( basepath, folder), widget, '**/*.html')))
		.pipe( flatmap( function( stream, file ){

			//var path = file.path.split( '/' );
			let path = underscorePathRexp.exec( file.path );
			let name;

			if( path && path[ 2 ] ) {
				//file name ( no extension ) will be used to name the template variable in the JS
				name = path[ 2 ].split( '.' )[0];
			}

			//wrap the contents in a string var declaration
			return stream.pipe( htmlmin( {
				collapseWhitespace: true,
				conservativeCollapse: true,
				removeComments: false
			} ) )
				.pipe( escape() )
				.pipe( insert.prepend( `PULSE.app.templates['${widget === '/' ? 'common' : widget}.${name}'] = _.template(` ) )
				.pipe( insert.append( ');' ) )
		}))
		.pipe(concat('concat.templates.js'))
		//get rid of any unnecessary formatting
		//.pipe(uglify())
		//pipe the generated variable declarations back into the widget directory
		.pipe(gulp.dest( path.join( path.join( basepath, folder), widget, "concat_tmpl" )))
};

import fs from 'fs';
import GulpSSH from 'gulp-ssh';
import config from '../config.json';

const pemLocation = './gulp/ssh/amazon-file-server.pem';

export function gulpSSH() {
    const sshConfig = {
        host: config.ftp.prod.host,
        port: 22,
        username: 'ec2-user',
        privateKey: fs.readFileSync( pemLocation )
    }

    return new GulpSSH({
        ignoreErrors: false,
        sshConfig: sshConfig
    });
}

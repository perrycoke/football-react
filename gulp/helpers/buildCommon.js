import babel from 'gulp-babel';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import fs from 'fs';
import path from 'path';
import rename from 'gulp-rename';
import sourcemaps from 'gulp-sourcemaps';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from '../config.json';
gulpParam(gulp, process.argv);

export function buildCommon( isProd = false, dist = false, basePath = config.src.common, includeTemplates = true ) {

    const structurePath = path.join( basePath, '_structure.txt');

    const jsSrc = includeTemplates ? [ path.join( basePath, 'concat_tmpl/concat.templates.js' ) ] : [];

    let hasStructure = true;

    try {
        fs.statSync(structurePath);
    } catch ( e ) {
        hasStructure = false;
    }

    if ( hasStructure ) {
        console.log( 'common: using _structure.txt' );
        let structure = fs.readFileSync(structurePath, 'utf8').split('\n');
        for ( let i = 0; i < structure.length; i++ ) {
            path.join(  )
            jsSrc.push(path.join( basePath, structure[ i ] ) );
        }
    } else {
        jsSrc.push( path.join( basePath, '**/*.js' ) );
    }

    if ( isProd ) {
        return gulp.src(jsSrc)
            .pipe(babel())
            .pipe(concat( 'common.js' ))
            .pipe(rename( 'common.min.js' ))
            .pipe(uglify())
            .pipe(gulp.dest(dist||config.dist.widgets));
    } else {
        return gulp.src(jsSrc)
            .pipe(sourcemaps.init())
            .pipe(babel())
            .pipe(concat( 'common.js' ))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(dist||config.dist.widgets));
    }
};

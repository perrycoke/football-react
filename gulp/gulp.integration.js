import del from 'del';
import runSequence from 'run-sequence';
import sourcemaps from 'gulp-sourcemaps';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import babel from 'gulp-babel';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import config from './config.json';
gulpParam(gulp, process.argv);

gulp.task( 'integration:clean', function( cb ) {
    return del( [ 'integration-files' ], cb );
} );


gulp.task( 'integration:html', function() {
    return gulp.src([
        './dist/html/aflm-web/integration.html'
	])
    .pipe( gulp.dest( './integration-files' ) )
});

gulp.task( 'integration:fonts', function() {
    return gulp.src([
    	'./dist/fonts/**/*'
	])
    .pipe( gulp.dest( `./integration-files/resources/${config.integration.resourceVersion}/fonts` ) )
});


gulp.task( 'integration:css', function() {
    return gulp.src([
    	'./dist/styles/integration.css'
	])
    .pipe( gulp.dest( `./integration-files/resources/${config.integration.resourceVersion}/styles` ) )
});

gulp.task( 'integration:sass', function() {
    return gulp.src([
        './src/styles/integration.scss'
    ])
    .pipe( gulp.dest( `./integration-files/resources/${config.integration.resourceVersion}/sass` ) )
});

gulp.task( 'integration:img:sprites', function() {
    return gulp.src([
        './dist/i/sprites/**/*',
	])
    .pipe( gulp.dest( `./integration-files/resources/${config.integration.resourceVersion}/i/sprites` ) )
});

gulp.task( 'integration:img:elements', function() {
    return gulp.src([
        './dist/i/elements/**/*',
	])
    .pipe( gulp.dest( `./integration-files/resources/${config.integration.resourceVersion}/i/elements` ) )
});


gulp.task( 'integration:svg', function() {
    return gulp.src([
        './dist/i/svg-output/*.svg'
	])
    .pipe( gulp.dest( `./integration-files/resources/${config.integration.resourceVersion}/i/svg-output` ) )
});

gulp.task( 'integration:svg:all', function() {
    return gulp.src([
        './src/i/svg-files/**/*.svg'
	])
    .pipe( gulp.dest( `./integration-files/resources/${config.integration.resourceVersion}/i/svg-files` ) )
});

gulp.task( 'integration:scripts', function() {
    return gulp.src([
        'src/widgets/sites/aflm/telstra-bar/js/telstra-bar-off-network.js'
    ])
    .pipe( babel() )
    .pipe( uglify() )
    .pipe( concat( 'telstra-bar.min.js' ) )
    .pipe( gulp.dest( `./integration-files/resources/${config.integration.resourceVersion}/scripts` ) )
});

gulp.task( 'integration:widgets', function() {
    return gulp.src([
        'dist/widgets/common.js'
	])
    .pipe( gulp.dest( `./integration-files/resources/${config.integration.resourceVersion}/widgets` ) )
});

gulp.task( 'integration', function() {
    runSequence( 'integration:clean', ['integration:html', 'integration:css', 'integration:sass', 'integration:img:sprites', 'integration:img:elements', 'integration:svg', 'integration:svg:all', 'integration:widgets', 'integration:scripts', 'integration:fonts'] );
} );


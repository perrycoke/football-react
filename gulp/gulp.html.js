import connect from 'gulp-connect';
import fileinclude from 'gulp-file-include';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from './config.json';
gulpParam(gulp, process.argv);

// HTML Build
gulp.task('html', function() {
  return gulp.src([config.src.html.pages])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: config.src.html.includes
    }))
    .pipe(gulp.dest(config.dist.html))
    .pipe(connect.reload());
});

// Update on change
gulp.task( 'html:watch', ['webserver'], function() {
    gulp.watch( config.src.html.folder, [ 'html' ] );
} );

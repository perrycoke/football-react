import Doc from 'gulp-jsdoc3';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
gulpParam(gulp, process.argv);

gulp.task( 'docs', function( cb ) {
	return gulp.src('./src/widgets/**/*.js')
		.pipe( Doc( cb ) );
} );

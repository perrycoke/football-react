import { publishFiles } from './helpers/publish-files.js';
import awspublish from 'gulp-awspublish';
import config from './config.json';
import rename from 'gulp-rename';
import path from 'path';
import gulp from 'gulp';

// Deploy to dev
gulp.task( 'deploy:dev', function() {
   return gulp.src( config.dist.src )
        .pipe(rename( function( filePath ) {
            filePath.dirname = path.join( config.s3.resources.dev.path, filePath.dirname );
        } ) )
        .pipe( publishFiles(
            config.s3.resources.dev.profile,
            config.s3.resources.dev.region,
            config.s3.resources.dev.bucket
        ) )
        .pipe( awspublish.reporter() );
} );

// Deploy SVGs to dev
gulp.task( 'deploy:svgs:dev', function() {

    return gulp.src( config.dist.svgs )
        .pipe( rename( function( filePath ) {
            filePath.dirname = path.join( config.s3.resources.dev.path, filePath.dirname );
        } ) )
        .pipe( publishFiles(
            config.s3.resources.dev.profile,
            config.s3.resources.dev.region,
            config.s3.resources.dev.bucket
        ) )
        .pipe( awspublish.reporter()
    );
} );

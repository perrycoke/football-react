import del from 'del';
import path from 'path';
import rename from 'gulp-rename';
import connect from 'gulp-connect';
import fileinclude from 'gulp-file-include';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from './config.json';
gulpParam(gulp, process.argv);

gulp.task('clean:ftl', function () {
    return del( config.dist.freemarker + '/**/*' );
});

// freemarker templates
gulp.task('ftl', [ 'clean:ftl' ], function() {
    return gulp.src([config.src.freemarker.src])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: config.src.freemarker.includes
        }))
        .pipe(rename(function (path) {
            if (path.extname) {
                if (path.dirname === '.') {
                    path.dirname = '';
                }

                const divider = path.dirname.indexOf( '\\' ) > -1 ? '\\' : '/';
                let dirs = path.dirname.replace( divider, '***' ).split( divider ).join( '_' );

                path.basename = dirs.replace( '***', divider ) + '_' + path.basename;
                path.dirname = '';
            }
        }))
        .pipe(gulp.dest(config.dist.freemarker))
        .pipe(connect.reload());
});

import del from 'del';
import bower from 'gulp-bower';
import color from 'gulp-color';
import connect from 'gulp-connect';
import { spawn } from 'child_process';
import runSequence from 'run-sequence';
import { gulpSSH } from './helpers/ssh.js';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from './config.json';
import cors from 'cors';
gulpParam(gulp, process.argv);

gulp.task( 'clean', function( cb ) {
    return del( [ config.dist.folder + '/*' ], cb );
} );

// Start a local server
gulp.task( 'webserver', function() {
    connect.server( {
        root: config.dist.folder,
        host: config.webserver.host,
        port: config.webserver.port,
        livereload: true,
        middleware: function() {
            return [cors()];
        }
    } );
} );

/**
 * Update deps according to semver in package.json
 */
gulp.task('update-deps', function() {
    var process = spawn( 'npm update', [], { stdio: 'inherit', shell: true });
    process.on( 'error', function ( err ) {
        console.log( err );
    });
});

gulp.task('manifest', function() {
    return gulp.src(config.src.manifest)
        .pipe(gulp.dest(config.dist.folder));
});

// Watch for local changes
gulp.task( 'watch', [
    'scripts:watch',
    'styles:watch',
    'img:watch',
    'svg:watch',
    'sprites:watch',
    'html:watch',
    'widgets:watch'
]);

/**
 * Working sequence
 */
gulp.task( 'default', function() {
    runSequence(
        'clean',
        [ 'sprites' ],
        [ 'update-deps', 'styles', 'ftl', 'widgets:build:dev', 'scripts:dev', 'html', 'i18n:build', 'svg', 'fonts' ],
        'vendor-files:dev',
        'sw',
        'manifest'
    );
} );

gulp.task( 'prod', function( ) {
    runSequence(
        'clean',
        [ 'sprites-prod' ],
        [ 'update-deps', 'styles-prod', 'ftl', 'fonts', 'svg', 'widgets:build:prod', 'scripts:prod', 'html', 'i18n:build' ],
        'vendor-files:prod',
        'sw:prod',
        'manifest'
    );
} );

gulp.task('prod:version', function(version) {
    if (!version || /^[a-zA-Z0-9]*$/.test(version) === false) {
        return console.error('Please provide a hash as follows: gulp prod:version --version gitHash');
    }

    var resourcesPath = config.ftp.prod.path;
    var versionedPath = resourcesPath + '/' + version;
    var symlinkedPath = resourcesPath + '/ver';

    var successMessage = '\nVersion changed successfully. The new version is "' + version + '".\n';
    var noVersionMessage = '\nVersion "' + version + '" not found. No action taken.\n';

    return gulpSSH().exec(['if test -d ' + versionedPath +
            '; then if ln -sfn ' + versionedPath + ' ' + symlinkedPath + '; then echo "success"; fi' +
            '; else echo "failure"; fi'
        ])
        .on('data', function(file) {
            var message = file.contents.toString();
            if (message.search('success') > -1) {
                console.log(color(successMessage, 'GREEN'));
            } else {
                console.log(color(noVersionMessage, 'RED'));
            }
        });
});

import babel from 'gulp-babel';
import runSequence from 'run-sequence';
import insert from 'gulp-insert';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';

import gulpParam from 'gulp-param';
import gulp from 'gulp';
gulpParam(gulp, process.argv);

import config from './config.json';

gulp.task('sw', function () {
    runSequence('sw:service-worker');
});

gulp.task('sw:prod', function () {
    runSequence('sw:service-worker:prod');
});

gulp.task('sw:service-worker', ['sw-get-workbox'], function() {
    const swVersion = Date.now();
    return gulp.src(config.src.sw)
        .pipe(insert.prepend( `const versionNumber = ${swVersion}; const debug = true;` ))
        .pipe(babel())
        .pipe(concat('sw.js'))
        .pipe(gulp.dest(config.dist.sw));
});

gulp.task('sw:service-worker:prod', ['sw-get-workbox'], function() {
    const swVersion = Date.now();
    return gulp.src(config.src.sw)
        .pipe(insert.prepend( `const versionNumber = ${swVersion}; const debug = false;` ))
        .pipe(babel())
        .pipe(uglify())
        .pipe(concat('sw.js'))
        .pipe(gulp.dest(config.dist.sw));
});

// gets workbox from node_modules and adds to dist. (no bower package available)
gulp.task('sw-get-workbox', function() {
    return gulp.src(config.src.workbox)
        .pipe(gulp.dest(config.dist.js));
});

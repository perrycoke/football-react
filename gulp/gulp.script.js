import fs from 'fs';
import del from 'del';
import path from 'path';
import babel from 'gulp-babel';
import insert from 'gulp-insert';
import concat from 'gulp-concat';
import rename from 'gulp-rename';
import uglify from 'gulp-uglify';
import sourcemaps from 'gulp-sourcemaps';
import gulpParam from 'gulp-param';
import env from 'gulp-environment';
import gulp from 'gulp';
import config from './config.json';
gulpParam(gulp, process.argv);


gulp.task( 'appconfig', function() {
    /**
     * fetch the individual path files from the directory
     * given in config and insert them into appconfig JSON array
     */
    var getPaths = function() {
        var dir = fs.readdirSync( config.src.scripts.paths );
        var concatenated = '';
        for( var g = 0; g < dir.length; g++ ) {
            var fp = config.src.scripts.paths + dir[ g ];
            if( fs.lstatSync( fp ).isFile() && path.extname( fp ) === '.json' ) {
                var content = fs.readFileSync( fp, "utf-8" );
                // convert to JSON so we can check the "label" for this path
                const config = JSON.parse( content );
                // conditionally include paths based on environment config.
                // alternatively, could check for production build and then exclude paths.
                if( config[ 'label' ] === env.current.name || env.current.aliases.includes( config[ 'label' ] ) ) {
                    concatenated += content;
                    if( g < dir.length - 1 ) {
                        concatenated += ',';
                    }
                }
            }
        }
        return concatenated;
    }

    return gulp.src( config.src.scripts.appconfig )
        .pipe( insert.transform( function( contents, file ) {
            contents = contents.replace( '//<PATHS>', getPaths() );
            return contents;
        } ) )
        .pipe( rename( 'appConfig.temp.js' ) )
        .pipe( gulp.dest( config.dist.js ) );

} )

// Join JS files
gulp.task( 'scripts:dev', [ 'appconfig' ], function() {
    return gulp.src( config.src.js )
        .pipe( sourcemaps.init() )
        .pipe( babel() )
        .on( 'error', function( error ) {
            console.log( error )
        } )
        .pipe( concat( 'main.js' ) )
        .pipe( sourcemaps.write() )
        .pipe( gulp.dest( config.dist.js ) )
        .pipe( concat( 'main.min.js' ) )
        .pipe( gulp.dest( config.dist.js ) )
        // remove the pre build temp version of appconfig when we are done
        .on( 'end', function() {
            del( config.dist.scripts.appconfig );
        } );
} );

gulp.task( 'scripts:prod', [ 'appconfig' ], function() {
return gulp.src( config.src.js )
    .pipe( babel() )
    .pipe( concat( 'main.js' ) )
    .pipe( gulp.dest( config.dist.js ) )
    .pipe( concat( 'main.min.js' ) )
    .pipe( uglify() )
    .pipe( gulp.dest( config.dist.js ) )
    // remove the pre build temp version of appconfig when we are done
    .on( 'end', function() {
        del( config.dist.scripts.appconfig );
    } );
} );


// Watch script src for changes
gulp.task( 'scripts:watch', [ 'scripts:dev' ], function() {
    gulp.watch( [ config.src.js ], [ 'scripts:dev' ] );
} );

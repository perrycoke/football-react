import eslint from 'gulp-eslint';
import eslintIfFixed from 'gulp-eslint-if-fixed';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from './config.json';
gulpParam(gulp, process.argv);

// Lint the js files
/**
 * Lints all the js files located in the config. It does a single loop to fix any issues it can automatically, before
 * having a second-pass and printing the results out. Any changes made are done in the source folder
 */
gulp.task('lint', function () {
    return gulp.src(config.src.allJs)
        .pipe(eslint({fix: true}))
        .pipe(eslintIfFixed(config.src.folder))
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console. Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on lint error, return the stream and pipe to failAfterError last.
        .pipe(eslint.failAfterError());
});

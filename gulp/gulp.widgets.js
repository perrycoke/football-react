import fs from 'fs';
import del from 'del';
import path from 'path';
import es from 'event-stream';
import runSequence from 'run-sequence';
import { buildCommon } from './helpers/buildCommon.js';
import { buildWidget } from './helpers/buildWidget.js';
import { getTemplateDeclarations } from './helpers/getTemplateDeclarations.js';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from './config.json';
gulpParam(gulp, process.argv);

function getFolders(dir){
    return fs.readdirSync(dir)
      .filter(function(file){
        return fs.statSync(path.join(dir, file)).isDirectory();
      });
}

gulp.task('common:templates', function() {
	return getTemplateDeclarations( "/", "/", config.src.common );
});

gulp.task('widgets:common:dev', [ "common:templates" ],  function() {
	return buildCommon(false);
});

gulp.task('widgets:common:prod', [ "common:templates" ],  function() {
	return buildCommon(true);
});

gulp.task('clean:templates', function () {
	//remove directories that contain concatenated template files
	let folders = getFolders(config.src.sites);
	folders.map(function(folder){
		let widgets = getFolders(path.join(config.src.sites, folder));
		widgets.map(function(widget){
			del([path.join(path.join(path.join(config.src.sites, folder), widget, "concat_tmpl/**" ))])
		});
	});

	//remove template directories from common widget dir
	del( path.join(config.src.common, 'concat_tmpl/**') );
});

gulp.task('widgets:templates', function() {
	const folders = getFolders(config.src.sites);

	let tasks = folders.map(function(folder){
		let widgets = getFolders(path.join(config.src.sites, folder));
		let items = widgets.map(function(widget) {
			return getTemplateDeclarations(folder, widget, config.src.sites);
		});
		return es.concat.apply(null, items);
	});
	return es.concat.apply(null, tasks);
});

gulp.task('widget:template', function( name ) {
	let folders = name.split('_');
	return getTemplateDeclarations(folders[0], folders[1], config.src.sites);
});

gulp.task('widgets:sites', ['widgets:templates'], function() {
	const folders = getFolders(config.src.sites);

	let tasks = folders.map(function(folder){
		let widgets = getFolders(path.join(config.src.sites, folder));
		let items = widgets.map(function(widget) {
		  return buildWidget(folder, widget, false);
		});
		return es.concat.apply(null, items);
	});

   return es.concat.apply(null, tasks);
});

gulp.task('widgets:sites:prod', ['widgets:templates'], function() {
	const folders = getFolders(config.src.sites);

	let tasks = folders.map(function(folder){
		let widgets = getFolders(path.join(config.src.sites, folder));
		let items = widgets.map(function(widget) {
		  return buildWidget(folder, widget, true);
		});
		return es.concat.apply(null, items);
	});

   return es.concat.apply(null, tasks);
});

gulp.task('widgets:build:dev', function() {
	//ensure tasks are run is sequence ( mainly for cleanup )
	runSequence( 'widgets:common:dev', 'widgets:sites', 'clean:templates' );
});

gulp.task('widgets:build:prod', function() {
	//ensure tasks are run is sequence ( mainly for cleanup )
	runSequence( 'widgets:common:prod', 'widgets:sites:prod', 'clean:templates' );
});

gulp.task('widget', [ 'widget:template' ], function( name ){
	let folders = name.split('_');
	return buildWidget(folders[0], folders[1]);
});

gulp.task('widget:build', function(){
	runSequence( 'widget', 'clean:templates');
});

gulp.task('widget',function( name ){
	let folders = name.split('_');
	return buildWidget(folders[0], folders[1]);
});

// Watch script src for changes
gulp.task( 'widgets:watch', [ 'widgets:build:dev' ], function() {
    gulp.watch( [ config.src.allJs, config.src.widgetTemplates ], [ 'widgets:build:dev' ] );
} );

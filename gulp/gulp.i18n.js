import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from './config.json';
gulpParam(gulp, process.argv);

// Deploy style resources
gulp.task( 'i18n:build', function() {
	return gulp.src( config.src.i18n + '*.js', {
				base: config.src.folder,
				buffer: false
			} )
			.pipe( gulp.dest( config.dist.folder ) );
} );

import uglify from 'gulp-uglify';
import svgSprite from 'gulp-svg-sprite';
import connect from 'gulp-connect';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from './config.json';
gulpParam(gulp, process.argv);

const svgConfig = {
    shape : {
        id : {
            separator : '-'
        },
        dimension       : {
            maxWidth    : 30,
            maxHeight   : 30
        }
    },
    mode : {
        symbol : {
            sprite : "../icons.svg",
            dimensionAttributes : true,
            example : {
                render : false,
                dest : '../../../html/svg-icons.html'
            }
        }
    },
    svg: {
        xmlDeclaration: false,
        doctypeDeclaration: false
    }
};

gulp.task('svg', function () {
    return gulp.src(config.src.svg.folder)
        .pipe(svgSprite(svgConfig))
        .pipe(gulp.dest(config.dist.svg))
        .pipe(connect.reload());
});

// Update on change
gulp.task( 'svg:watch', ['webserver'], function() {
    gulp.watch( config.src.svg.folder, [ 'svg' ] );
} );

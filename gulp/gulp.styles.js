import del from 'del';
import sass from 'gulp-sass';
import nano from 'gulp-cssnano';
import connect from 'gulp-connect';
import imagemin from 'gulp-imagemin';
import gulpIgnore from 'gulp-ignore';
import sassGlob from 'gulp-sass-glob';
import runSequence from 'run-sequence';
import sourcemaps from 'gulp-sourcemaps';
import pngquant from 'imagemin-pngquant';
import autoprefixer from 'gulp-autoprefixer';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from './config.json';
gulpParam(gulp, process.argv);

const compileStyles = () => {
    return gulp.src(config.src.style.css)
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.dist.css))
        .pipe(connect.reload());
}

// Compile CSS
gulp.task('styles:css', compileStyles );

// Compile production ready CSS
gulp.task('styles:css-prod', function () {
    return gulp.src(config.src.style.css)
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        .pipe(nano({
            zindex: false
        }))
        .pipe(autoprefixer())
        .pipe(gulp.dest(config.dist.css));
});

// Update styles on change
gulp.task('styles:watch', [ 'styles:css' ], function () {
    gulp.watch([config.src.style.css, config.src.style.components], [ 'styles:css' ]);
});

// Build Fonts
gulp.task( 'fonts', function() {
    return gulp.src( config.src.fonts )
        .pipe( gulp.dest( config.dist.fonts ) );
} );

// Build images
gulp.task( 'styles:img', function() {
    return gulp.src( config.src.style.img )
        .pipe(gulpIgnore.exclude(['sprite-files/**', 'svg-files/**/*']))
        .pipe( gulp.dest( config.dist.img ) );
} );

// Build images
gulp.task('styles:img-prod', function () {
    return gulp.src(config.src.style.img)
        .pipe(gulpIgnore.exclude([ 'sprite-files/**', 'temp/*' ]))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [ { removeViewBox: false } ],
            use: [ pngquant() ]
        }))
        .pipe( gulp.dest( config.dist.img ) );
} );

// Update images on chang
gulp.task('img:watch', [ 'styles', 'webserver' ], function () {
    gulp.watch("src/i/**/*", [ 'styles:img' ]);
});

// Run all styles tasks
gulp.task('styles', [ 'styles:css', 'styles:img' ]);

// Run all production styles tasks
gulp.task( 'styles-prod', [ 'styles:css-prod', 'styles:img-prod' ] );

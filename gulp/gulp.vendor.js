import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import runSequence from 'run-sequence';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from './config.json';
gulpParam(gulp, process.argv);


/**
 * gulp vendor-files
 *
 * Takes vendor files globs `config.src.vendorbundle`. Will concat and
 * distribute to `config.dist.js` as:
 *
 *  1. `vendors.js` should contain non-minified vendor JS files
 *  2. `vendors.min.js` should contain minified vendor JS files (if vendor
 *      provides them)
 */
gulp.task( 'vendor-files', function() {
    runSequence( 'vendor-files:dev', 'vendor-files:prod' );
});

gulp.task( 'vendor-files:dev', function() {
    return gulp.src( config.src.vendorBundle.development )
        .pipe( concat( 'vendors.js' ) )
        .pipe( gulp.dest( config.dist.js ) );
});

/**
 * All files passed in the vendorbundle.production glob should be minified, but
 * sometimes they aren't. This is why there's an uglify call to ensure minified
 * output.
 */
gulp.task( 'vendor-files:prod', function() {
    return gulp.src( config.src.vendorBundle.production )
        .pipe( concat( 'vendors.min.js' ) )
        .pipe( uglify() )
        .pipe( gulp.dest( config.dist.js ) );
});

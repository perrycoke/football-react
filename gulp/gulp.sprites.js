'use strict';
import del from 'del';
import fs from 'fs';
import path from 'path';
import es from 'event-stream';
import merge from 'merge-stream';
import rename from 'gulp-rename';
import replace from 'gulp-replace';
import connect from 'gulp-connect';
import imagemin from 'gulp-imagemin';
import pngquant from 'imagemin-pngquant';
import spritesmith from 'gulp.spritesmith';
import imageResize from 'gulp-image-resize';
import buffer from 'vinyl-buffer';
import gulpParam from 'gulp-param';
import gulp from 'gulp';
import config from './config.json';
gulpParam(gulp, process.argv);

// Sprite generation variables
var folders = [],
    spriteSuffix = '-sprite',
    includes = '',
    spriteMin = false;

// Method to get array of folders from file system based on given path
function getFolders( dir ){
  return fs.readdirSync(dir)
    .filter( function( file ){
      return fs.statSync( path.join( dir, file )).isDirectory();
    } );
};

// Clean Task
gulp.task( 'sprites:clean', function () {
  return del( [ config.dist.sprite.style, './src/i/sprite-files/*/temp/' ] );
} );

gulp.task( 'sprites:clean-temp', ['sprites:generate'], function () {
  return del( './src/i/sprite-files/*/temp/' );
} );

// Sprites Generation

gulp.task( 'sprites:duplicate', ['sprites:clean'], function() {

  folders = getFolders( config.src.sprite.img );

  var task = folders.map( function( folder ){

      return gulp.src('src/i/sprite-files/' + folder + '/*.png')
      .pipe(imageResize({
        width : '50%',
        crop : false,
        upscale : false
      }))
      .pipe(rename( function(path) {
          path.basename += '-non-retina';
      }))
      .pipe(gulp.dest('./src/i/sprite-files/' + folder + '/temp'));
    })

  return es.concat.apply(null, task);

})

gulp.task( 'sprites:generate', ['sprites:duplicate'], function() {
  folders = getFolders( config.src.sprite.img );

  var task = folders.map( function( folder ){

    var spriteOptions = {
      retinaSrcFilter: config.src.sprite.img + folder + '/*.png',
      imgName: folder + spriteSuffix + '.png',
      retinaImgName: folder + spriteSuffix + '@x2.png',
      imgPath: config.dist.sprite.path + folder + spriteSuffix + '.png',
      retinaImgPath : config.dist.sprite.path + folder + spriteSuffix + '@x2.png',
      cssName: '_' + folder + '.scss',
      cssTemplate: config.src.sprite.templates + folder + spriteSuffix
    };

    var spriteData = gulp.src( config.src.sprite.img + folder + '/**/*.png' )
        .pipe( spritesmith( spriteOptions ) );

    if ( spriteMin ) {
        var cssStream = spriteData.css
            .pipe( replace( '-non-retina', '' ) )
            .pipe( gulp.dest( config.dist.sprite.style ) );
        var imgStream = spriteData.img.pipe( buffer() ).pipe( imagemin( [
            imagemin.gifsicle(),
            imagemin.jpegtran( { progressive: true } ),
            pngquant(),
            imagemin.svgo( {
                plugins: [
                    { removeViewBox: false }
                ]
            } )
        ], {
            verbose: true
        } ) )
            .pipe(gulp.dest( config.dist.sprite.img ) );
    } else {
        var cssStream = spriteData.css
            .pipe( replace( '-non-retina', '' ) )
            .pipe( gulp.dest( config.dist.sprite.style ) );
        var imgStream = spriteData.img.pipe(gulp.dest( config.dist.sprite.img ) );
    }

    return merge(imgStream, cssStream);
  });

  return merge.apply( null, task );
});

// Creates SASS Include File
gulp.task('sprites', ['sprites:clean-temp'], function(){

  folders.map(function( folder ){
    includes += '@import "' + folder + '";';
  });

  fs.writeFile( config.src.sprite.includes, includes, () => {} );
});

gulp.task('set-production', function() {
  spriteMin = true;
});

// Sprite Main Function
gulp.task( 'sprites-prod', ['set-production', 'sprites:clean-temp'], function () {

  folders.map(function( folder ){
      includes += '@import "' + folder + '";';
  });

  fs.writeFile( config.src.sprite.includes, includes, () => {} );
} );

// Update on change
gulp.task( 'sprites:watch', ['webserver'], function() {
    gulp.watch( 'src/i/sprite-files/**/*', [ 'sprites' ] );
} );

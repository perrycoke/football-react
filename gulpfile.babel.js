'use strict';
require( './gulp/gulp.base.js' );
require( './gulp/gulp.script.js' );
require( './gulp/gulp.styles.js' );
require( './gulp/gulp.html.js' );
require( './gulp/gulp.sprites.js' );
require( './gulp/gulp.svg.js' );
require( './gulp/gulp.widgets.js' );
require( './gulp/gulp.i18n.js' );
require( './gulp/gulp.freemarker.js' );
require( './gulp/gulp.docs.js' );
require( './gulp/gulp.lint.js' );
require( './gulp/gulp.vendor.js' );
require( './gulp/gulp.sw.js' );
require( './gulp/gulp.integration.js' );
